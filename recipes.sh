#! /bin/bash


post_install () {
	AddHook
	mkinitcpio --allpresets
}


post_upgrade () {
	post_install
}


post_remove () {
	RemoveHook
	mkinitcpio --allpresets
}


AddHook () {
	if ! grep --quiet "openswap" "etc/mkinitcpio.conf"; then
		sed "s|encrypt |encrypt openswap resume |" --in-place "/etc/mkinitcpio.conf"
	fi
}


RemoveHook () {
	sed "s|encrypt openswap resume |encrypt |" --in-place "/etc/mkinitcpio.conf"
}
