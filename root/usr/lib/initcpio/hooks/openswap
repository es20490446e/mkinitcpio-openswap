#! /bin/bash
#  shellcheck disable=SC1090,SC2086,SC2154

conf="/etc/openswap.conf"


run_hook () {
	if [[ -f "${conf}" ]]; then
		source "${conf}"
	
		if [[ -n "${swap_device}" ]] && [[ -n "${crypt_swap_name}" ]]; then
			wait_keyfile_device
			open_swap
		fi
	fi
}


make_keyfile_detection_automatic () {
	sed \
		--expression 's|keyfile_device=.*|keyfile_device=""|' \
		--expression 's|keyfile_filename=.*|keyfile_filename=""|' \
		--in-place "${conf}"
	
	unset keyfile_device
	unset keyfile_filename
}


open_swap () {
	if [[ -z "${keyfile_device}" ]] || [[ -z "${keyfile_filename}" ]]; then
		open_swap_with_auto_keyfile
	else
		open_swap_with_conf_keyfile
	fi
}


open_swap_with_auto_keyfile () {
	cryptsetup open \
		${cryptsetup_options} \
		"${swap_device}" \
		"${crypt_swap_name}"
}


open_swap_with_conf_keyfile () {
	mkdir openswap_keymount
	mount ${keyfile_device_mount_options} "${keyfile_device}" openswap_keymount
	
	cryptsetup open \
		${cryptsetup_options} \
		--key-file "openswap_keymount/${keyfile_filename}" \
		"${swap_device}" \
		"${crypt_swap_name}"
	
	umount openswap_keymount
}


wait_keyfile_device () {	
	if [[ -n "${keyfile_device}" ]]; then
		local attempt=0
		local max=40
		
		while [[ ! -b "${keyfile_device}" ]] && [[ "${attempt}" -le "${max}" ]]; do
			sleep 0.1
			attempt="$((attempt + 1))"
		done
		
		if [[ "${attempt}" -eq "${max}" ]]; then			
			make_keyfile_detection_automatic
		fi
	fi
}
